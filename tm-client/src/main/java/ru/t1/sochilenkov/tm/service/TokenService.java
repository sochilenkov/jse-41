package ru.t1.sochilenkov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.sochilenkov.tm.api.service.ITokenService;

@Getter
@Setter
@NoArgsConstructor
public final class TokenService implements ITokenService {

    @NotNull
    public String token;

}
