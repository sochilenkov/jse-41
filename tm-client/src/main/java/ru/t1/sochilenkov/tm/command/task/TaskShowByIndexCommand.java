package ru.t1.sochilenkov.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.dto.request.TaskShowByIndexRequest;
import ru.t1.sochilenkov.tm.dto.model.TaskDTO;
import ru.t1.sochilenkov.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Show task by index.";

    @NotNull
    public static final String NAME = "task-show-by-index";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;

        @NotNull final TaskShowByIndexRequest request = new TaskShowByIndexRequest(getToken());
        request.setIndex(index);

        @Nullable final TaskDTO task = getTaskEndpoint().showTaskByIndex(request).getTask();
        showTask(task);
    }

}
