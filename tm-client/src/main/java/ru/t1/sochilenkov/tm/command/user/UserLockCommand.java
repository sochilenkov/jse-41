package ru.t1.sochilenkov.tm.command.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.sochilenkov.tm.dto.request.UserLockRequest;
import ru.t1.sochilenkov.tm.util.TerminalUtil;

public final class UserLockCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "Lock user in application.";

    @NotNull
    public static final String NAME = "user-lock";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[USER LOCK]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();

        @NotNull final UserLockRequest request = new UserLockRequest(getToken());
        request.setLogin(login);

        getUserEndpoint().lockUser(request);
    }

}
