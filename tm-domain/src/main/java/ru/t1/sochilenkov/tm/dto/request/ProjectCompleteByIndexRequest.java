package ru.t1.sochilenkov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectCompleteByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    public ProjectCompleteByIndexRequest(@Nullable final String token) {
        super(token);
    }

}
