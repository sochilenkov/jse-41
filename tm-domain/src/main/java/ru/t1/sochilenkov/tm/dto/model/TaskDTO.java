package ru.t1.sochilenkov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.api.model.IWBS;
import ru.t1.sochilenkov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tm_task")
public final class TaskDTO extends AbstractUserOwnedModelDTO implements IWBS {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(nullable = false, name = "name")
    private String name = "";

    @NotNull
    @Column(nullable = true, name = "description")
    private String description = "";

    @NotNull
    @Column
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "project_id")
    private String projectId;

    @NotNull
    @Column(name = "created")
    private Date created = new Date();

    @NotNull
    @Override
    public String toString() {
        return this.getName() + " | Status: " + status.getDisplayName() + " |";
    }

}
