package ru.t1.sochilenkov.tm.api.service;

import org.apache.ibatis.session.SqlSession;

import javax.persistence.EntityManagerFactory;

public interface IConnectionService {

    SqlSession getSqlSession();

    EntityManagerFactory getEMFactory();

}
