package ru.t1.sochilenkov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.dto.model.ProjectDTO;

import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO tm_project (id, name, created, description, user_id, status)" +
            " VALUES (#{project.id}, #{project.name}, #{project.created}, #{project.description}, #{userId}, #{project.status})")
    void add(@NotNull @Param("userId") String userId, @NotNull @Param("project") ProjectDTO project);

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId}")
    void clear(@NotNull @Param("userId") String userId);

    @Select("SELECT EXISTS(SELECT 1 FROM tm_project WHERE user_id = #{userId} AND id = #{id})")
    boolean existsById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Select("SELECT id, name, created, description, user_id, status FROM tm_project WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @NotNull List<ProjectDTO> findAll(@NotNull @Param("userId") String userId);

    @Select("SELECT id, name, created, description, user_id, status FROM tm_project WHERE user_id = #{userId}" +
            " ORDER BY name")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @NotNull List<ProjectDTO> findAllOrderName(@NotNull @Param("userId") String userId);

    @Select("SELECT id, name, created, description, user_id, status FROM tm_project WHERE user_id = #{userId}" +
            " ORDER BY created")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @NotNull List<ProjectDTO> findAllOrderCreated(@NotNull @Param("userId") String userId);

    @Select("SELECT id, name, created, description, user_id, status FROM tm_project WHERE user_id = #{userId}" +
            " ORDER BY status")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @NotNull List<ProjectDTO> findAllOrderStatus(@NotNull @Param("userId") String userId);

    @Select("SELECT id, name, created, description, user_id, status FROM tm_project WHERE user_id = #{userId} AND id = #{id}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable ProjectDTO findOneById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Select("SELECT id, name, created, description, user_id, status FROM tm_project WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable ProjectDTO findOneByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Select("SELECT count(id) FROM tm_project WHERE user_id = #{userId}")
    int getSize(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId} AND id = #{project.id}")
    void remove(@NotNull @Param("userId") String userId, @NotNull @Param("project") ProjectDTO project);

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId} AND id = #{id}")
    void removeById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Update("UPDATE tm_project SET name = #{project.name}, created = #{project.created}, description = #{project.description}," +
            " user_id = #{project.userId}, status = #{project.status} WHERE id = #{project.id}")
    void update(@NotNull @Param("project") ProjectDTO project);

}
