package ru.t1.sochilenkov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.api.service.IAuthService;
import ru.t1.sochilenkov.tm.api.service.IPropertyService;
import ru.t1.sochilenkov.tm.api.service.ISessionService;
import ru.t1.sochilenkov.tm.api.service.IUserService;
import ru.t1.sochilenkov.tm.enumerated.Role;
import ru.t1.sochilenkov.tm.exception.field.LoginEmptyException;
import ru.t1.sochilenkov.tm.exception.field.PasswordEmptyException;
import ru.t1.sochilenkov.tm.exception.system.AccessDeniedException;
import ru.t1.sochilenkov.tm.exception.system.AuthenticationException;
import ru.t1.sochilenkov.tm.dto.model.SessionDTO;
import ru.t1.sochilenkov.tm.dto.model.UserDTO;
import ru.t1.sochilenkov.tm.util.CryptUtil;
import ru.t1.sochilenkov.tm.util.HashUtil;

import java.util.Date;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    final IPropertyService propertyService;

    @NotNull
    private final ISessionService sessionService;

    public AuthService(
            @NotNull final IUserService userService,
            @NotNull final IPropertyService propertyService,
            @NotNull final ISessionService sessionService
    ) {
        this.userService = userService;
        this.propertyService = propertyService;
        this.sessionService = sessionService;
    }

    @NotNull
    @Override
    public String login(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDTO user = userService.findByLogin(login);
        if (user == null) throw new AuthenticationException();
        if (user.getLocked()) throw new AuthenticationException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new AuthenticationException();
        if (!hash.equals(user.getPasswordHash())) throw new AuthenticationException();
        return getToken(user);
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final UserDTO user) {
        return getToken(createSession(user));
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final SessionDTO session) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        return CryptUtil.encrypt(sessionKey, token);
    }

    @NotNull
    private SessionDTO createSession(@NotNull final UserDTO user) {
        @NotNull final SessionDTO session = new SessionDTO();
        session.setUserId(user.getId());
        @NotNull final Role role = user.getRole();
        session.setRole(role);
        return sessionService.add(session);
    }

    @Override
    public void invalidate(@Nullable final SessionDTO session) {
        if (session == null) return;
        sessionService.remove(session);
    }

    @NotNull
    @Override
    @SneakyThrows
    public SessionDTO validateToken(@Nullable String token) {
        if (token == null) throw new AccessDeniedException();
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull String json;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException();
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final SessionDTO session = objectMapper.readValue(json, SessionDTO.class);

        @NotNull final Date currentDate = new Date();
        @NotNull final Date sessionDate = session.getDate();
        final long delta = (currentDate.getTime() - sessionDate.getTime()) / 1000;
        final int timeout = propertyService.getSessionTimeout();
        if (delta > timeout) throw new AccessDeniedException();

        if (!sessionService.existsById(session.getId())) throw new AccessDeniedException();

        return session;
    }

    @NotNull
    @Override
    public UserDTO registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        return userService.create(login, password, email);
    }

}
