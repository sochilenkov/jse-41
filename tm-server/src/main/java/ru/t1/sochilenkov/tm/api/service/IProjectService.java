package ru.t1.sochilenkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.enumerated.Sort;
import ru.t1.sochilenkov.tm.enumerated.Status;
import ru.t1.sochilenkov.tm.dto.model.ProjectDTO;

import java.util.List;

public interface IProjectService {

    void clear(@Nullable final String userId);

    @NotNull
    List<ProjectDTO> findAll(@Nullable final String userId);

    boolean existsById(@Nullable final String userId, @Nullable final String id);

    @Nullable
    ProjectDTO findOneById(@Nullable final String userId, @Nullable final String id);

    @Nullable
    ProjectDTO findOneByIndex(@Nullable final String userId, @Nullable final Integer index);

    void removeById(@Nullable final String userId, @Nullable final String id);

    void removeByIndex(@Nullable final String userId, @Nullable final Integer index);

    @NotNull
    List<ProjectDTO> findAll(@Nullable final String userId, @Nullable final Sort sort);

    @NotNull
    ProjectDTO changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    ProjectDTO changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    void add(@Nullable String userId, @Nullable ProjectDTO project);

    void updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    void updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

}
