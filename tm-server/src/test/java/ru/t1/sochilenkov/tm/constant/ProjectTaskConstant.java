package ru.t1.sochilenkov.tm.constant;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.UUID;

public interface ProjectTaskConstant {

    int INIT_COUNT_TASKS = 5;

    @NotNull
    String USER_ID_1 = "tst-usr-pr-tsk-id-1";

    @NotNull
    String USER_ID_2 = "tst-usr-pr-tsk-id-2";

    @Nullable
    String NULLABLE_USER_ID = null;

    @Nullable
    String EMPTY_USER_ID = "";

    @Nullable
    String NULLABLE_PROJECT_ID = null;

    @NotNull
    String EMPTY_PROJECT_ID = "";

    @Nullable
    String NULLABLE_TASK_ID = null;

    @NotNull
    String EMPTY_TASK_ID = "";


}
