package ru.t1.sochilenkov.tm.repository;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.sochilenkov.tm.api.repository.IUserRepository;
import ru.t1.sochilenkov.tm.api.service.IConnectionService;
import ru.t1.sochilenkov.tm.api.service.IPropertyService;
import ru.t1.sochilenkov.tm.marker.UnitCategory;
import ru.t1.sochilenkov.tm.dto.model.UserDTO;
import ru.t1.sochilenkov.tm.service.ConnectionService;
import ru.t1.sochilenkov.tm.service.PropertyService;

import java.util.*;

import static ru.t1.sochilenkov.tm.constant.UserConstant.*;

@Category(UnitCategory.class)
public class UserRepositoryTest {

    @NotNull
    private IUserRepository repository;

    @NotNull
    private List<UserDTO> userList;

    @NotNull
    private static SqlSession sqlSession;

    @BeforeClass
    public static void changeSchema() {
        System.setProperty("database.schema", "tm-tst");
        @NotNull IPropertyService propertyService = new PropertyService();
        @NotNull IConnectionService connectionService = new ConnectionService(propertyService);
        sqlSession = connectionService.getSqlSession();
    }

    @Before
    public void init() {
        repository = sqlSession.getMapper(IUserRepository.class);
        userList = new ArrayList<>();
        repository.clear();
        for (int i = 1; i <= INIT_COUNT_USERS; i++) {
            @NotNull final UserDTO user = new UserDTO();
            user.setLogin("User_" + i);
            user.setEmail("User_" + i + "@test.ru");
            user.setFirstName("User_" + i);
            user.setLastName("Userovov_" + i);
            user.setLastName("Userovich_" + i);
            repository.add(user);
            userList.add(user);
        }
    }

    @After
    public void ClearAfter() {
        repository.clear();
        sqlSession.commit();
    }

    @AfterClass
    public static void closeConnection() {
        sqlSession.close();
    }

    @Test
    public void testAddUserPositive() {
        UserDTO user = new UserDTO();
        user.setLogin("UserAddTest");
        repository.add(user);
        Assert.assertEquals(INIT_COUNT_USERS + 1, repository.getSize());
    }

    @Test
    public void testClear() {
        Assert.assertEquals(INIT_COUNT_USERS, repository.getSize());
        repository.clear();
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testFindById() {
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString()));
        for (@NotNull final UserDTO user : userList) {
            final UserDTO foundUser = repository.findOneById(user.getId());
            Assert.assertNotNull(foundUser);
            Assert.assertEquals(user.getId(), foundUser.getId());
        }
    }

    @Test
    public void testExistsById() {
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString()));
        for (@NotNull final UserDTO user : userList) {
            Assert.assertTrue(repository.existsById(user.getId()));
        }
    }

    @Test
    public void testFindByIndex() {
        Assert.assertNull(repository.findOneByIndex(9999));
        for (final UserDTO user : userList) {
            final UserDTO foundUser = repository.findOneByIndex(userList.indexOf(user));
            Assert.assertNotNull(foundUser);
            Assert.assertEquals(user.getId(), foundUser.getId());
        }
    }

    @Test
    public void testFindAll() {
        List<UserDTO> users = repository.findAll();
        Assert.assertNotNull(users);
        Assert.assertEquals(userList.size(), users.size());
        for (final UserDTO user : userList) {
            Assert.assertEquals(user.getId(), users.get(userList.indexOf(user)).getId());
        }
    }

    @Test
    public void testRemoveByIdPositive() {
        Assert.assertEquals(INIT_COUNT_USERS, repository.getSize());
        for (final UserDTO user : userList) {
            repository.removeById(user.getId());
            Assert.assertNull(repository.findOneById(user.getId()));
        }
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testRemovePositive() {
        Assert.assertEquals(INIT_COUNT_USERS, repository.getSize());
        for (final UserDTO user : userList) {
            repository.remove(user);
        }
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void testExistsLogin() {
        for (final UserDTO user : userList) {
            Assert.assertTrue(repository.isLoginExist(user.getLogin()));
        }
    }

    @Test
    public void testFindByLogin() {
        for (final UserDTO user : userList) {
            Assert.assertNotNull(repository.findByLogin(user.getLogin()));
        }
    }

    @Test
    public void testExistsEmail() {
        for (final UserDTO user : userList) {
            Assert.assertTrue(repository.isEmailExist(user.getEmail()));
        }
    }

    @Test
    public void testFindByEmail() {
        for (final UserDTO user : userList) {
            Assert.assertNotNull(repository.findByEmail(user.getEmail()));
        }
    }

}
