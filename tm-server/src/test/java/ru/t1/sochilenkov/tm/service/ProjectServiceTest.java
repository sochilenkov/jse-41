package ru.t1.sochilenkov.tm.service;

import lombok.Synchronized;
import lombok.ToString;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.sochilenkov.tm.api.service.IConnectionService;
import ru.t1.sochilenkov.tm.api.service.IProjectService;
import ru.t1.sochilenkov.tm.api.service.IPropertyService;
import ru.t1.sochilenkov.tm.constant.ProjectConstant;
import ru.t1.sochilenkov.tm.dto.model.TaskDTO;
import ru.t1.sochilenkov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.sochilenkov.tm.exception.field.*;
import ru.t1.sochilenkov.tm.marker.UnitCategory;
import ru.t1.sochilenkov.tm.dto.model.ProjectDTO;

import java.util.*;

import static ru.t1.sochilenkov.tm.constant.ProjectConstant.*;
import static ru.t1.sochilenkov.tm.constant.ProjectConstant.CREATED_SORT;
import static ru.t1.sochilenkov.tm.constant.ProjectConstant.IN_PROGRESS_STATUS;
import static ru.t1.sochilenkov.tm.constant.ProjectConstant.NULLABLE_SORT;
import static ru.t1.sochilenkov.tm.constant.ProjectConstant.EMPTY_USER_ID;
import static ru.t1.sochilenkov.tm.constant.ProjectConstant.NULLABLE_INDEX;
import static ru.t1.sochilenkov.tm.constant.ProjectConstant.NULLABLE_USER_ID;
import static ru.t1.sochilenkov.tm.constant.ProjectConstant.INIT_COUNT_PROJECTS;

@Category(UnitCategory.class)
public class ProjectServiceTest {

    @NotNull
    private static IProjectService projectService;

    @NotNull
    private List<ProjectDTO> projectList;

    public static String USER_ID_1 = "tst-usr-project-id-1-1";

    public static String USER_ID_2 = "tst-usr-project-id-1-2";

    public static long USER_ID_COUNTER = 0;

    @BeforeClass
    public static void changeSchema() {
        System.setProperty("database.schema", "tm-tst");
        @NotNull IPropertyService propertyService = new PropertyService();
        @NotNull IConnectionService connectionService = new ConnectionService(propertyService);
        projectService = new ProjectService(connectionService);
    }

    @Before
    public void init() {
        USER_ID_COUNTER++;
        USER_ID_1 = "tst_usr_for_index_test_1_" + USER_ID_COUNTER;
        USER_ID_2 = "tst_usr_for_index_test_2_" + USER_ID_COUNTER;
        projectList = new ArrayList<>();
        for (int i = 1; i <= INIT_COUNT_PROJECTS; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Project_" + i);
            project.setDescription("Description_" + i);
            project.setUserId(USER_ID_1);
            projectList.add(project);
            projectService.add(USER_ID_1, project);
        }
        for (int i = 1; i <= INIT_COUNT_PROJECTS; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Project_" + i);
            project.setDescription("Description_" + i);
            project.setUserId(USER_ID_2);
            projectList.add(project);
            projectService.add(USER_ID_2, project);
        }
    }

    @After
    public void closeConnection() {
        projectService.clear(USER_ID_1);
        projectService.clear(USER_ID_2);
    }

    @Test
    public void testClearPositive() {
        Assert.assertEquals(INIT_COUNT_PROJECTS, projectService.findAll(USER_ID_1).size());
        projectService.clear(USER_ID_1);
        Assert.assertEquals(0, projectService.findAll(USER_ID_1).size());
        Assert.assertEquals(INIT_COUNT_PROJECTS, projectService.findAll(USER_ID_2).size());
        projectService.clear(USER_ID_2);
        Assert.assertEquals(0, projectService.findAll(USER_ID_2).size());
    }

    @Test
    public void testClearNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.clear(EMPTY_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.clear(NULLABLE_USER_ID));
    }

    @Test
    public void testFindAllNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll(EMPTY_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll(NULLABLE_USER_ID));
    }

    @Test
    public void testFindAllPositive() {
        @NotNull List<ProjectDTO> projects = projectService.findAll(USER_ID_1);
        Assert.assertNotNull(projects);
        for (final ProjectDTO project : projectList) {
            if (project.getUserId().equals(USER_ID_1))
                Assert.assertNotNull(
                        projects.stream()
                                .filter(m -> project.getUserId().equals(m.getUserId()))
                                .filter(m -> project.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
        }
        projects = projectService.findAll(USER_ID_2);
        Assert.assertNotNull(projects);
        for (final ProjectDTO project : projectList) {
            if (project.getUserId().equals(USER_ID_2))
                Assert.assertNotNull(
                        projects.stream()
                                .filter(m -> project.getUserId().equals(m.getUserId()))
                                .filter(m -> project.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
        }
        projects = projectService.findAll(UUID.randomUUID().toString());
        Assert.assertEquals(Collections.emptyList(), projects);
    }

    @Test
    public void testFindAllSortNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll(NULLABLE_USER_ID, CREATED_SORT));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll(EMPTY_USER_ID, CREATED_SORT));
    }

    @Test
    public void testFindAllSortPositive() {
        List<ProjectDTO> projects = projectService.findAll(USER_ID_1, CREATED_SORT);
        Assert.assertNotNull(projects);
        for (final ProjectDTO project : projectList) {
            if (project.getUserId().equals(USER_ID_1))
                Assert.assertNotNull(
                        projects.stream()
                                .filter(m -> project.getUserId().equals(m.getUserId()))
                                .filter(m -> project.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
        }
        projects = projectService.findAll(USER_ID_1, NAME_SORT);
        Assert.assertNotNull(projects);
        for (final ProjectDTO project : projectList) {
            if (project.getUserId().equals(USER_ID_1))
                Assert.assertNotNull(
                        projects.stream()
                                .filter(m -> project.getUserId().equals(m.getUserId()))
                                .filter(m -> project.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
        }
        projects = projectService.findAll(USER_ID_1, STATUS_SORT);
        Assert.assertNotNull(projects);
        for (final ProjectDTO project : projectList) {
            if (project.getUserId().equals(USER_ID_1))
                Assert.assertNotNull(
                        projects.stream()
                                .filter(m -> project.getUserId().equals(m.getUserId()))
                                .filter(m -> project.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
        }
        projects = projectService.findAll(USER_ID_2, NULLABLE_SORT);
        Assert.assertNotNull(projects);
        for (final ProjectDTO project : projectList) {
            if (project.getUserId().equals(USER_ID_2))
                Assert.assertNotNull(
                        projects.stream()
                                .filter(m -> project.getUserId().equals(m.getUserId()))
                                .filter(m -> project.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
        }
    }

    @Test
    public void testAddProjectNegative() {
        @NotNull final ProjectDTO project = new ProjectDTO();
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.add(NULLABLE_USER_ID, project));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.add(EMPTY_USER_ID, project));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.add(USER_ID_1, NULLABLE_PROJECT));
    }

    @Test
    public void testAddProjectPositive() {
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName("ProjectAddTest");
        project.setDescription("ProjectAddTest desc");
        projectService.add(USER_ID_1, project);
        Assert.assertEquals(INIT_COUNT_PROJECTS + 1, projectService.findAll(USER_ID_1).size());
        project.setId(UUID.randomUUID().toString());
    }

    @Test
    public void testExistsByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.existsById(NULLABLE_USER_ID, projectList.get(0).getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.existsById(EMPTY_USER_ID, projectList.get(0).getId()));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.existsById(USER_ID_1, NULLABLE_PROJECT_ID));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.existsById(USER_ID_1, EMPTY_PROJECT_ID));
    }

    @Test
    public void testExistsByIdPositive() {
        for (final ProjectDTO project : projectList) {
            Assert.assertTrue(projectService.existsById(project.getUserId(), project.getId()));
        }
    }

    @Test
    public void testFindOneByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findOneById(NULLABLE_USER_ID, projectList.get(0).getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findOneById(EMPTY_USER_ID, projectList.get(0).getId()));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.findOneById(USER_ID_1, NULLABLE_PROJECT_ID));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.findOneById(USER_ID_1, EMPTY_PROJECT_ID));
    }

    @Test
    public void testFindOneByIdPositive() {
        for (final ProjectDTO project : projectList) {
            Assert.assertEquals(project.getId(), projectService.findOneById(project.getUserId(), project.getId()).getId());
        }
    }

    @Test
    public void testFindOneByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findOneByIndex(NULLABLE_USER_ID, 0));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findOneByIndex(EMPTY_USER_ID, 0));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.findOneByIndex(USER_ID_1, NULLABLE_INDEX));
    }

    @Test
    public void testFindOneByIndexPositive() {
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            Assert.assertEquals(projectList.get(i).getId(), projectService.findOneByIndex(USER_ID_1, i).getId());
        }
        for (int i = 5; i < INIT_COUNT_PROJECTS * 2; i++) {
            Assert.assertEquals(projectList.get(i).getId(), projectService.findOneByIndex(USER_ID_2, i - 5).getId());
        }
    }

    @Test
    public void testRemoveByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.removeById(NULLABLE_USER_ID, NULLABLE_PROJECT_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.removeById(EMPTY_USER_ID, NULLABLE_PROJECT_ID));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.removeById(USER_ID_1, NULLABLE_PROJECT_ID));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.removeById(USER_ID_1, EMPTY_PROJECT_ID));
    }

    @Test
    public void testRemoveByIdPositive() {
        for (final ProjectDTO project : projectList) {
            projectService.removeById(project.getUserId(), project.getId());
            Assert.assertFalse(projectService.findAll(project.getUserId()).contains(project));
        }
        Assert.assertEquals(0, projectService.findAll(USER_ID_1).size());
        Assert.assertEquals(0, projectService.findAll(USER_ID_2).size());
    }

    @Test
    public void testRemoveByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.removeByIndex(NULLABLE_USER_ID, NULLABLE_INDEX));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.removeByIndex(EMPTY_USER_ID, NULLABLE_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.removeByIndex(USER_ID_1, NULLABLE_INDEX));
    }

    @Test
    public void testCreateProjectNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.create(NULLABLE_USER_ID, null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.create(EMPTY_USER_ID, null, null));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.create(USER_ID_1, null, null));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.create(USER_ID_1, "", null));
    }

    @Test
    public void testCreateProjectPositive() {
        Assert.assertEquals(INIT_COUNT_PROJECTS, projectService.findAll(USER_ID_1).size());
        projectService.create(USER_ID_1, "PROJ", "PROJ_DESC");
        Assert.assertEquals(INIT_COUNT_PROJECTS + 1, projectService.findAll(USER_ID_1).size());

        Assert.assertEquals(INIT_COUNT_PROJECTS, projectService.findAll(USER_ID_2).size());
        projectService.create(USER_ID_2, "PROJ", "PROJ_DESC");
        Assert.assertEquals(INIT_COUNT_PROJECTS + 1, projectService.findAll(USER_ID_2).size());

        Assert.assertEquals(INIT_COUNT_PROJECTS + 1, projectService.findAll(USER_ID_1).size());
        projectService.create(USER_ID_1, "PROJ_2", "");
        Assert.assertEquals(INIT_COUNT_PROJECTS + 2, projectService.findAll(USER_ID_1).size());

        Assert.assertEquals(INIT_COUNT_PROJECTS + 1, projectService.findAll(USER_ID_2).size());
        projectService.create(USER_ID_2, "PROJ_2", "");
        Assert.assertEquals(INIT_COUNT_PROJECTS + 2, projectService.findAll(USER_ID_2).size());

        Assert.assertEquals(INIT_COUNT_PROJECTS + 2, projectService.findAll(USER_ID_1).size());
        projectService.create(USER_ID_1, "PROJ_3", null);
        Assert.assertEquals(INIT_COUNT_PROJECTS + 3, projectService.findAll(USER_ID_1).size());
    }

    @Test
    public void testChangeProjectStatusByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusById(NULLABLE_USER_ID, null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusById(EMPTY_USER_ID, null, null));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.changeProjectStatusById(USER_ID_1, null, null));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.changeProjectStatusById(USER_ID_1, "", null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.changeProjectStatusById(USER_ID_1, UUID.randomUUID().toString(), null));
        Assert.assertThrows(StatusIncorrectException.class, () -> projectService.changeProjectStatusById(USER_ID_1, projectService.findOneByIndex(USER_ID_1, 0).getId(), null));
    }

    @Test
    public void testChangeProjectStatusByIdPositive() {
        for (final ProjectDTO project : projectList) {
            Assert.assertNotNull(projectService.changeProjectStatusById(project.getUserId(), project.getId(), IN_PROGRESS_STATUS));
            Assert.assertEquals(IN_PROGRESS_STATUS, projectService.findOneById(project.getUserId(), project.getId()).getStatus());
        }
    }

    @Test
    public void testChangeProjectStatusByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusByIndex(NULLABLE_USER_ID, NULLABLE_INDEX, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusByIndex(EMPTY_USER_ID, NULLABLE_INDEX, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.changeProjectStatusByIndex(USER_ID_1, NULLABLE_INDEX, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.changeProjectStatusByIndex(USER_ID_1, -1, null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.changeProjectStatusByIndex(UUID.randomUUID().toString(), 0, null));
    }

    @Test
    public void testChangeProjectStatusByIndexPositive() {
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            projectService.changeProjectStatusByIndex(USER_ID_1, i, IN_PROGRESS_STATUS);
        }
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            ProjectDTO project = projectList.get(i);
            Assert.assertNotNull(projectService.findAll(USER_ID_1).stream()
                    .filter(m -> project.getId().equals(m.getId()))
                    .filter(m -> IN_PROGRESS_STATUS.equals(m.getStatus()))
                    .findFirst()
                    .orElse(null));
        }
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            projectService.changeProjectStatusByIndex(USER_ID_2, i, IN_PROGRESS_STATUS);
        }
        for (int i = 5; i < INIT_COUNT_PROJECTS + 5; i++) {
            ProjectDTO project = projectList.get(i);
            Assert.assertNotNull(projectService.findAll(USER_ID_2).stream()
                    .filter(m -> project.getId().equals(m.getId()))
                    .filter(m -> IN_PROGRESS_STATUS.equals(m.getStatus()))
                    .findFirst()
                    .orElse(null));
        }
    }

    @Test
    public void testUpdateByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateById(NULLABLE_USER_ID, null, null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateById(EMPTY_USER_ID, null, null, null));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.updateById(USER_ID_1, null, null, null));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.updateById(USER_ID_1, "", null, null));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateById(USER_ID_1, UUID.randomUUID().toString(), null, null));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateById(USER_ID_1, UUID.randomUUID().toString(), "", null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.updateById(USER_ID_1, UUID.randomUUID().toString(), "UPD_PROJ_1", null));
    }

    @Test
    public void testUpdateByIdPositive() {
        for (final ProjectDTO project : projectList) {
            projectService.updateById(project.getUserId(), project.getId(), project.getName(), null);
            projectService.updateById(project.getUserId(), project.getId(), project.getName(), "");
            projectService.updateById(project.getUserId(), project.getId(), project.getName() + "_upd", project.getDescription() + "_upd");
            Assert.assertEquals(project.getId(), projectService.findOneById(project.getUserId(), project.getId()).getId());
            Assert.assertEquals(project.getName() + "_upd", projectService.findOneById(project.getUserId(), project.getId()).getName());
            Assert.assertEquals(project.getDescription() + "_upd", projectService.findOneById(project.getUserId(), project.getId()).getDescription());
        }
    }

    @Test
    public void testUpdateByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateByIndex(NULLABLE_USER_ID, null, null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateByIndex(EMPTY_USER_ID, null, null, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.updateByIndex(USER_ID_1, null, null, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.updateByIndex(USER_ID_1, -1, null, null));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateByIndex(USER_ID_1, 0, null, null));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateByIndex(USER_ID_1, 0, "", null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.updateByIndex(UUID.randomUUID().toString(), 0, "UPD_PROJ_1", null));
    }

    @Test
    public void testUpdateByIndexPositive() {
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            ProjectDTO project = projectList.get(i);
            projectService.updateByIndex(project.getUserId(), i, project.getName(), null);
            projectService.updateByIndex(project.getUserId(), i, project.getName(), "");
            projectService.updateByIndex(project.getUserId(), i, project.getName() + "_upd", project.getDescription() + "_upd");
            project.setName(project.getName() + "_upd");
            Assert.assertNotNull(
                    projectService.findAll(project.getUserId()).stream()
                            .filter(m -> project.getUserId().equals(m.getUserId()))
                            .filter(m -> project.getName().equals(m.getName()))
                            .findFirst()
                            .orElse(null)
            );
        }
        for (int i = 5; i < INIT_COUNT_PROJECTS * 2; i++) {
            ProjectDTO project = projectList.get(i);
            projectService.updateByIndex(project.getUserId(), i - 5, project.getName(), null);
            projectService.updateByIndex(project.getUserId(), i - 5, project.getName(), "");
            projectService.updateByIndex(project.getUserId(), i - 5, project.getName() + "_upd", project.getDescription() + "_upd");
            project.setName(project.getName() + "_upd");
            Assert.assertNotNull(
                    projectService.findAll(project.getUserId()).stream()
                            .filter(m -> project.getUserId().equals(m.getUserId()))
                            .filter(m -> project.getName().equals(m.getName()))
                            .findFirst()
                            .orElse(null)
            );
        }
    }

}
