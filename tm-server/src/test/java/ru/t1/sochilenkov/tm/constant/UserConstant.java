package ru.t1.sochilenkov.tm.constant;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.enumerated.Role;
import ru.t1.sochilenkov.tm.dto.model.UserDTO;

public interface UserConstant {

    int INIT_COUNT_USERS = 5;

    @Nullable
    UserDTO NULLABLE_USER = null;

    @Nullable
    String NULLABLE_USER_ID = null;

    @Nullable
    String EMPTY_USER_ID = "";

    @Nullable
    Integer NULLABLE_INDEX = null;

    @Nullable
    String NULLABLE_EMAIL = null;

    @NotNull
    String EMPTY_EMAIL = "";

    @Nullable
    String NULLABLE_LOGIN = null;

    @NotNull
    String EMPTY_LOGIN = "";

    @Nullable
    String NULLABLE_PASSWORD = null;

    @NotNull
    String EMPTY_PASSWORD = "";

    @Nullable
    Role NULLABLE_ROLE = null;

}
